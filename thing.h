#ifndef THING_H
#define THING_H

#include <string>
using namespace std;
class Room;
class Thing{

	private:
		string name;
		string desc;
		int size;
	protected:
		Room* cur_room;

	public:
		Thing(string _name, string _desc, int _size);
		bool moveTo(Room* r);
		string getName();
		string getDesc();
		int getSize();
};

#endif
