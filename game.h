#ifndef GAME_H
#define GAME_H
#include "agent.h"
#include <vector>
using namespace std;
class Game{
	private:
		vector<Agent*> agents;

	public:
		Game();
		void addAgent(Agent* _agent);
		bool step();
};
#endif
