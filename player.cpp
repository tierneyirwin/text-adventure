#include "player.h"
#include "thing.h"
#include "room.h"
#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <vector>
#include <sstream>
#include <istream>
#include "stdio.h"
#include <fstream>
#include <iterator>
using namespace std;
Player::Player(string _name, string _description, int _size)
	: Agent(_name, _description, _size)
{}

bool Player::act(){
	cout << "What should we do, " << getName() << "?" << endl;
	vector<string> vect;
	string str;
	getline(cin,str);
	istringstream iss(str);
	copy(istream_iterator<string>(iss),istream_iterator<string>(),back_inserter(vect));
	if(vect.empty()){
		cout << "We'll move on from this...dalliance... as if you meant to do nothing." << endl;
		return true;
	}
		if(vect[0] == "quit"){
			return false;
		}else if(vect[0] == "inventory"){
			if(cur_room->getThings().size() == 0){
				cout << "The thing of which you desire was never there to begin with." << endl;
				return true;
			}
			set<Thing*>::iterator it;
			for(it = inventory.begin(); it != inventory.end(); ++it){
				cout<< "You are holding these items:  " << endl;	
				cout << (*it)-> getName() << endl;
				cout << "Congrats, you can hold things!" << endl;
			}		
			return true;
		}else if(vect[0] == "look"){
			cout << cur_room->getDesc() << endl;
			cout << "You can see that all of these things are in this room!" << endl;
			cur_room->printThings(this);
			cout << "You 'could' go to these places...if you dare..." << endl;
			cur_room->printExits();
			return true;
		}else if(vect[0] == "break"){
			cout << "Don't be rude! Plus that is unbreakable despite what you thought. " << endl;
			return true;
		}
		else if(vect[0] == "go"){
			map<string, Room*> exitz = cur_room->getExits();
			map<string,Room*>::iterator pos;
			pos = exitz.find(vect[1]);
			if(pos != exitz.end()){
				walk(vect[1]); 
				return true;
			} else {
				cout <<"No, bad person. You don't even go here." <<endl;
				return true;
			}
		}else if(vect[0] == "take"){
			set<Thing*>::iterator it;
			set<Thing*> take = cur_room->getThings();
			for(it = take.begin(); it != take.end(); ++it){
				if((*it)->getName() == vect[1]){
					break;
				}
			}
			if(it != take.end()){
				addInventory((*it));
				return true;
			}		
			cout << "That's not a thing, silly goose!" << endl;
			return true;
		}else if(vect[0] == "get"){
                        set<Thing*>::iterator it;
			set<Thing*> take = cur_room->getThings();
                        for(it = take.begin(); it != take.end(); ++it){
				if((*it)->getName() == vect[1]){
					break;
				}
			}
                        if(it != take.end()){
                                addInventory((*it));
                                return true;
                        }
			cout << "How dare you! I'll be keeping that!" << endl;
                        return true;
	
		}else if(vect[0] == "touch"){
			string place = cur_room->getName();
			if(place == "Entrance"){
				if(vect[1] == "floor" || vect[1] == "floorboards"){
					cout << "Yea, it's some pretty cherry wood alright. Little dusty though." <<endl;
					return true;
				}
				if(vect[1] == "statue"){
					cout << "CRASH! ...Well now look what you did! ...She was perfectly fine until you did that!" << endl;
					set<Thing*> remove = cur_room->getThings();
					set<Thing*>::iterator it;
					for(it = remove.begin(); it != remove.end(); ++it){
						if((*it)->getName() == "statue"){
							cur_room->remove((*it));
							break;
						}
					}
					return true;
				}
			}else if(place == "study"){
				cout << "Did you read the sign? Can you read? No...touching.... plain and simple. Look at yourself, you got a paper cut!" << endl;
				return true;
			}else if(place == "eatery"){
				cout << "A knight approaches you, waving a short stick at you. 'You there! How about you not touch our setting! This is for the party tonight! Who are you anyway!' You quickly scurry out of the room, fearing the stick will impale your delicate skin." << endl;
				return walk("north");
			}else{
				cout << "No touchy." << endl;
				return true;
			}
		}else if(vect[0] == "drop"){
			if(inventory.size() == 0){
				cout <<"You've got nothing to lose. You're in the danger zone." << endl;
				return true;
			}
			cout << "You are keeping this forever. Take responsibility for your actions." << endl;
			return true;
		}else{
			cout << "Just no, I forbid you." << endl;
			return true;
		}
		
	return false;
}
bool Player::walk(string exit){
	map<string,Room*>::iterator pos;
	map<string,Room*> _exit = cur_room->getExits();
	pos = _exit.find(exit);
	if(pos != _exit.end()){
		cout << "You were in: " << cur_room->getName() << " and now currently reside in " << (pos)->second->getName() << endl;
		moveTo((pos)->second);
		return true;
	}else{
		cout << "So.... where are you trying to go?" << endl;
		return true;;
	}
	cout << "didnt make it through the war" << endl;
	return false;	
}
void Player::addInventory(Thing* thing){
	inventory.insert(thing);
	cur_room->remove(thing);
}
set<Thing*> Player::getInventory(){
	return inventory;
}
