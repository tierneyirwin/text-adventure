#include "thing.h"
#include "room.h"
using namespace std;
Thing::Thing(string _name, string _desc,int _size)
 :name(_name), desc(_desc), size(_size),cur_room(NULL)
{}

bool Thing::moveTo(Room* r){
	if(r->add(this)){
		if(cur_room != NULL){
			cur_room->remove(this);
		}
		cur_room = r;
		return true;
	}
	return false;
} 

string Thing::getName()
{
	return name;
}

string Thing::getDesc(){
	return desc;
}

int Thing::getSize(){
	return size;
}
