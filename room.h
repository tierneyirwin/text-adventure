#ifndef ROOM_H
#define ROOM_H
#include <string>
#include <set>
#include <vector>
#include <map>
using namespace std;
class Thing;
class Room{
	private:
		string name;
		string description;
		int size;
		set<Thing*> thingsInRoom;
		map<string,Room*> exits;

	public:
		Room(string name, string desc, int size);
		string getName() const;
		string getDesc();
		void link(Room* room, string exit);
		map<string,Room*> getExits();
		bool add(Thing* thing);
		void remove(Thing* thing);
		void printExits();
		void printThings(Thing* ignore);
		set<Thing*> getThings();
};
#endif
