#ifndef PLAYER_H
#define PLAYER_H
#include "agent.h"
#include "room.h"
#include <set>
#include <map>
#include <string>
#include <iostream>
using namespace std;
class Player : public Agent {
	private:
		set<Thing*> inventory;
	public: 
		Player(string name, string description, int size);
		bool act();
		bool walk(string exit);
		void addInventory(Thing* thing);
		set<Thing*> getInventory();
};
#endif
