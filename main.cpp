#include "thing.h"
#include "monster.h"
#include "player.h"
#include "game.h"
#include <iostream>
using namespace std;
int main()
{
    Game game;

    Room *entrance = new Room("Entrance",
                              "A wide open entrance welcoming you to the home. The walls are decorated with photos of people you will never care to remember. All you care to look at is the pretty cherry wood floorboards. If you were to be bothered, you would see a small lovely replica of our great nation's symbol, the statue of liberty. You know, you 'would' have seen that, had those darn floorboards not been so pretty and cherry.", 100);
    Room *hallway = new Room("SouthHall",
                             "You enter the hallway southbound from the entrance. Upon entering, you feel what was the hard floor crush underneath your feet into plush carpet. To think people still own shag carpeting is scary enough yet all around you hang the trophies of hunters, their prized kills mounted on a wall. You quickly avert your gaze forward, the feeling of their beady animal eyes forever imprinted on your psyche. Ahead rests three doors. To the east, a pair of dark oak doors. To the south, ornate white stained doors. To the west, a lowly swinging door. Above each door rests the room behind them but you need to look a bit harder for that. You forgot your glasses today, didn't you?", 50);
    Room *ballroom = new Room("Ballroom",
                              "Of course! A ballroom! A lovely open area for dancing and entertain-....wait, there are just....balls. Everywhere. A circus ball is the most prominent to your eye as its stripes are burning your retinas. I think whoever lives here didn't know the true purpose of a ballroom. Still fun....I guess?", 200);
    Room *kitchen = new Room("Kitchen", "There's a stove and fridge and yada yada yada... you know the drill by now. A kitchen where you make food. If you look around maybe you'll find something, if you want to be bothered to do so.", 150);

    Room* diningroom = new Room("Eatery", "At the end of the hall way rests a fine eatery! Well now! A 'Knights of the Round Table' type of table rests smack-dab in the center of this room. There is still plenty of room to shimmy around the chairs, which those chairs. The chairs surrounding the table are fit for lords and ladies! Plush backings and rustic metal ornaments along the sides. Truly not fit for someone of your station so no touching!", 200);
    Room* hall2 = new Room("WestHall", "Striding into the west wing of the building you enter the west hall. The windows along the outer wall allow bright sunshine to cascade upon you. THis is a surprisingly glorious room! (If you haven't seen the other rooms, ignore that bit) The opposite wall is a lovely robin's egg blue with children's drawings pinned to the wall. The floor has various childhood memories across it with crayon and marker stains forever drawn into the packed carpeting. How quaint.", 50);
    Room* study = new Room("Study", "Books, books and more books! I would call this a library yet the door clearly said study so I'm not arguing that one. In the center lies a hearty desk with a comfy armchair behind. The armchair appears velvety and the desk has papers thrown all over it. A no touching sign is posted on the desk, they mean it!", 100);
    Room* easthall = new Room("EastHall", "As you walk toward the east wing of this crazy house, you notice this side of the house is dimmer than the other side. The sun is casted away from this side so your sight is primarily still useful by the candlelights hanging from the ceiling. ", 50);
    Room* bathroom = new Room("Bathroom", "I would hope you know what a bathroom looks like...", 100);
    Room* bedroom = new Room("Bedroom", "There's beds...and sheets on beds....and it is a room...Think about it....Look around", 200);

    entrance->link(hallway, "south");
    hallway->link(entrance, "north");
    hallway->link(ballroom, "east");
    ballroom->link(hallway, "west");
    kitchen->link(hallway,"east");
    hallway->link(kitchen,"west");
    entrance->link(hall2, "west");
    hall2->link(entrance,"east");
    study->link(hall2,"east");
    hall2->link(study,"west");
    hallway->link(diningroom,"south");
    diningroom->link(hallway,"north");
    entrance->link(easthall,"east");
    easthall->link(entrance,"west");
    bathroom->link(easthall,"north");
    easthall->link(bathroom,"south");
    bedroom->link(easthall,"west");
    easthall->link(bedroom,"east");
       

    Player *josh = new Player("Josh", "A prince", 50);
    Player *tracy = new Player("Tracy", "A princess", 40);
    josh->moveTo(entrance);
    tracy->moveTo(entrance);
    game.addAgent(josh);
    game.addAgent(tracy);

    Monster *napolean = new Monster("Napolean");
    Monster *kafka = new Monster("Kafka");
    Monster *boo = new Monster("Boo");
    boo->moveTo(bathroom);
    napolean->moveTo(ballroom);
    kafka->moveTo(ballroom);
    game.addAgent(napolean);
    game.addAgent(kafka);
    game.addAgent(boo);

    Thing *liberty = new Thing("statue",
                               "A miniature Statue of Liberty", 5);
    Thing *hoop = new Thing("hoop", "A basketball hoop", 30);
    Thing* ball = new Thing("ball", "A giant circus ball", 30);
    Thing* wrench = new Thing("wrench", "A monkey wrench", 10);
    Thing* doll = new Thing("doll", "A child's doll", 10);
    Thing* chickencut = new Thing("chicken", "A piece of chicken, raw", 5);
    Thing* knife = new Thing("knife", "A chef's knife stabbed into a cutting board", 10);
    Thing* bust = new Thing("bust", "A marble bust of some old important person, idk", 15);
    Thing* book = new Thing("book", "Dictionary of words with pictures of those words.", 10);
    Thing* vase = new Thing("vase", "seems breakable", 10);
   
    vase->moveTo(hall2);
    book->moveTo(study);
    bust->moveTo(easthall);
    knife->moveTo(kitchen);
    chickencut->moveTo(kitchen);
    doll->moveTo(bedroom);
    wrench->moveTo(hall2);
    ball->moveTo(ballroom);
    liberty->moveTo(entrance);
    hoop->moveTo(ballroom);

    cout << "Welcome! There is nothing here for you. Letting you know now before you get disappointed." << endl;
    while(game.step());
    
	delete hall2;
	delete diningroom;
	delete kitchen;
	delete entrance;
	delete hallway;
	delete ballroom;
	delete study;
	delete easthall;
	delete bathroom;
	delete bedroom;
	delete liberty;
	delete hoop;
	delete ball;
	delete wrench;
	delete doll;
	delete chickencut;
	delete knife;
	delete bust;
	delete book;
	delete vase;
	delete napolean;
	delete josh;
	delete tracy;
	delete kafka;
	delete boo;	
	return 0;
}
