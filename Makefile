
myprog: main.o room.o thing.o monster.o player.o game.o agent.o
	g++ -Wall -ansi -pedantic -g -O2 -o myprog main.o room.o thing.o monster.o player.o game.o agent.o

main.o: main.cpp game.h player.h monster.h 
	g++ -Wall -ansi -pedantic -g -O2 -c main.cpp

room.o: room.cpp room.h thing.h
	g++ -Wall -ansi -pedantic -g -O2 -c room.cpp

thing.o: thing.cpp thing.h room.h
	g++ -Wall -ansi -pedantic -g -O2 -c thing.cpp

monster.o: monster.cpp agent.cpp monster.h thing.h agent.h
	g++ -Wall -ansi -pedantic -g -O2 -c monster.cpp

player.o: player.cpp agent.cpp player.h thing.h room.h agent.h
	g++ -Wall -ansi -pedantic -g -O2 -c player.cpp

agent.o: agent.cpp agent.h thing.h
	g++ -Wall -ansi -pedantic -g -O2 -c agent.cpp 

game.o: game.cpp game.h agent.h
	g++ -Wall -ansi -pedantic -g -O2 -c game.cpp

all: myprog

.PHONY: clean
clean:
	rm -f *.o myprog
