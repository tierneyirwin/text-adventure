#include "room.h"
#include "thing.h"
#include "game.h"
#include <map>
#include <string>
#include <set>
#include <iostream>
using namespace std;
Room::Room(string _name, string _de, int _size){
	name = _name;
	description = _de;
	size = _size;
}

string Room::getName() const{
	return name;
}

string Room::getDesc(){
	return description;
}

void Room::link(Room* _room, string exit){
	exits[exit] = _room;
}

map<string,Room*> Room::getExits(){
	return exits;
}

bool Room::add(Thing* thing){
	thingsInRoom.insert(thing);
	return true;
}

void Room::remove(Thing* thing){
	set<Thing*>::iterator it;
	it = thingsInRoom.find(thing);
	thingsInRoom.erase(it,thingsInRoom.end());
}

void Room::printExits(){
	map<string,Room*>::iterator it;
	for(it = exits.begin();it != exits.end(); ++it){
		cout << it->first << " -> " << it->second->getName() << endl;
	}
}

void Room::printThings(Thing* ignore){
	set<Thing*>::iterator it;
	for(it = thingsInRoom.begin(); it != thingsInRoom.end();++it){
		if(*it == ignore){
			
		}else{
			cout << (*it)->getName() << endl;
		}
	}
	
}

set<Thing*> Room::getThings(){
//	vector<string> str;
//	for(int i = 0; i< thingsInRoom.size()	
	return thingsInRoom;
}
