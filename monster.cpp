#include "monster.h"
#include "thing.h"
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string>
#include "room.h"
#define NULL 0
using namespace std;
Monster::Monster(string _name)
	:Agent(_name, " ", NULL)
{}

bool Monster::act(){
	map<string,Room*> _exits = cur_room->getExits();
	map<string,Room*>::iterator it;
	int x = rand() % _exits.size();
	for(it = _exits.begin(); it != _exits.end(); ++it){
		int count = 0;
		if(count == x){
			moveTo(it->second);
			return true;
		}
		count++;
	}	
	return true;
}
bool Monster::walk(string _exit){
	map<string, Room*>::iterator pos;
	pos = cur_room->getExits().find(_exit);
	if(pos != cur_room->getExits().end()){
		moveTo(pos->second);
		return true;
	}
	return true;
}
//do i need walk?
	

