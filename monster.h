#ifndef MONSTER_H
#define MONSTER_H
#include "agent.h"
class Monster: public Agent {
	public:
		Monster(string name);
		bool walk(string exit);
		bool act();
};
#endif
